$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#view-book').click(function(){
        var id = $(this).attr('data-id'); 
        $.ajax(
            {
                url: "/count-view/"+id,
                type: 'GET', // replaced from put
                dataType: "JSON",
                data: {
                    "id": id // method and token not needed in data
                },
                success: function (response)
                {
                    console.log(response); // see the reponse sent
                },
                error: function(xhr) {
                    console.log(xhr.responseText); // this line will save you tons of hours while debugging
                }
            });
    });


    $("#btn-download").click(function(){
        var id = $(this).attr('data-id'); 
        $.ajax(
            {
                url: "/count-download/"+id,
                type: 'GET', // replaced from put
                dataType: "JSON",
                data: {
                    "id": id // method and token not needed in data
                },
                success: function (response)
                {
                    console.log(response); // see the reponse sent
                },
                error: function(xhr) {
                    console.log(xhr.responseText); // this line will save you tons of hours while debugging
                }
            });
    });
});

$(document).ready(function(){
    
    var slider = $(".owl-carousel");
    var loop = slider.data("loop");
    var nav = slider.data('nav');
    var dots = slider.data('dots');
    var autoplay = slider.data('autoplay');
    var autoplaytimeout =  slider.data('autoplaytimeout');
  $(".owl-carousel").owlCarousel({
        loop: loop,
        margin:5,
        nav: nav,
        dots: dots,
        autoplay: autoplay,
        autoplayTimeout: autoplaytimeout,
        autoplayHoverPause:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    });
});