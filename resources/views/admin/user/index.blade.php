@extends('admin.fragments.master')

@section('breadcrumb')
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active">User</li>
    </ol>
@endsection

@section('content')
<div class="container-fluid pb-4">
    
    <div class="card mb-5">
        <div class="card-header">
          <a href="{{route('admin.user.create')}}" class="btn btn-primary text-white"><i class="fa fa-plus"></i> New User</a>
        </div>
        <div class="card-body pl-0 pr-0">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Username</th>
                  <th>Email</th>
                  <th>Car Count</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Username</th>
                  <th>Email</th>
                  <th>Car Count</th>
                  <th>Action</th>
                </tr>
              </tfoot>
              <tbody>
                @if(isset($users) && count($users) > 0)
                    @foreach($users as $key => $user)
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td class="text-center h5"><span class=" badge badge-info">{{count($user->cars)}}</span></td>
                            <td>
                                {{-- <a href="#" data-id="55" class="btn btn-danger btn-sm" id="delete"><i class="fa fa-trash-o "></i></a> --}}
                                {!!Form::open(['route' => ['admin.user.destroy', $user->id], 'method' => 'POST', 'class' => 'd-inline'])!!}
                                   {{Form::hidden('_method','DELETE')}}
                                    @if(count($users) > 1)
                                    <button type="submit" data-id={{$user->id}} class="btn btn-danger btn-sm btn-delete-user" id="delete" ><i class="fa fa-trash-o "></i></button>
                                    @endif
                                {!!Form::close()!!}
                            </td>
                        </tr>
                    @endforeach
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>     
</div>
@endsection