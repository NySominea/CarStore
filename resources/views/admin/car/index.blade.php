@extends('admin.fragments.master')

@section('breadcrumb')
<!-- Breadcrumbs-->
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item active">Car</li>
</ol>
@endsection

@section('content')
<div class="container-fluid pb-4" id="car-list">
    <div class="card mb-5">
        <div class="card-header">
          <a href="{{route('admin.car.create')}}" class="btn btn-primary text-white"><i class="fa fa-plus"></i> New Car</a>
        </div>
        <div class="card-body">
            <div class="row mb-3">
                <div class="col-sm-12 col-md-4">
                  <div class="form-inline" >
                    <label>Show &nbsp; 
                      <select id="brand_size" name="size_table" class="form-control form-control-sm">
                        <option {{isset($_GET['size']) && $_GET['size'] == 10 ? 'selected' : ''}} value="size=10">10</option>
                        <option {{isset($_GET['size']) && $_GET['size'] == 25 ? 'selected' : ''}} value="size=25">25</option>
                        <option {{isset($_GET['size']) && $_GET['size'] == 50 ? 'selected' : ''}} value="size=50">50</option>
                        <option {{isset($_GET['size']) && $_GET['size'] == 100 ? 'selected' : ''}} value="size=100">100</option>
                      </select> &nbsp; entries
                    </label>
                  </div>
                </div> 
                <div class="col-sm-12 col-md-8 text-right">
                  <form class="form-inline" method="GET" action="{{route('admin.car.index')}}">
                        <label class="sr-only" for="car_name">car Title</label>
                        <input type="text" name="search" class="form-control mb-2 mr-sm-3" id="car-name" placeholder="Search for ..."
                                value="{{isset($_GET['search']) ? $_GET['search'] : ''}}">
                        
                        <label class="sr-only" for="status">Brand</label>
                        <select id="car-status" name="car_brand" class="form-control mb-2 mr-sm-3">
                            <option value="all">All Brand</option>
                            @foreach($brands as $brand)
                              <option {{isset($_GET['car_brand']) && $_GET['car_brand'] == $brand->slug ? 'selected' : ''  }} value="{{$brand->slug}}">{{$brand->name}}</option>  
                            @endforeach
                        </select> 

                        <label class="sr-only" for="status">Status</label>
                        <select id="car-status" name="car_status" class="form-control mb-2 mr-sm-3">
                            <option value="all">Status</option>
                            <option {{isset($_GET['car_status']) && $_GET['car_status'] == "enable" ? 'selected'  : ''}} value="enable">Enable</option>
                            <option {{isset($_GET['car_status']) && $_GET['car_status'] == "disable" ? 'selected' : ''}} value="disable">Disable</option>
                        </select> 
                      
                        <button type="submit" class="btn btn-primary mb-2"><i class="fa fa-search"></i></button>
                      </form>
                </div>
              </div>
          <div class="table-responsive">
            <table class="table table-bordered  table-hover">
              <thead>
                <tr>
                  <th>Image</th>
                  <th>Name <small class="d-none d-md-inline"><a href="sort=name&order={{isset($_GET['order'])?$_GET['order']=='asc'?'desc':'asc' :'asc'}}"  class="text-dark sort"><i class="fa {{getSortIcon('name')}}"></i></a></small></th>
                  <th>Brand <small class="d-none d-md-inline"><a href="sort=brand&order={{isset($_GET['order'])?$_GET['order']=='asc'?'desc':'asc' :'asc'}}"  class="text-dark sort"><i class="fa {{getSortIcon('brand')}}"></i></a></small></th>
                  <th>Price($) <small class="d-none d-md-inline"><a href="sort=price&order={{isset($_GET['order'])?$_GET['order']=='asc'?'desc':'asc' :'asc'}}"  class="text-dark sort"><i class="fa {{getSortIcon('price')}}"></i></a></small></th>
                  <th>Year <small class="d-none d-md-inline"><a href="sort=model_year&order={{isset($_GET['order'])?$_GET['order']=='asc'?'desc':'asc' :'asc'}}"  class="text-dark sort"><i class="fa {{getSortIcon('model_year')}}"></i></a></small></th>
                  <th>Condition <small class="d-none d-md-inline"><a href="sort=condition&order={{isset($_GET['order'])?$_GET['order']=='asc'?'desc':'asc' :'asc'}}"  class="text-dark sort"><i class="fa {{getSortIcon('condition')}}"></i></a></small></th>
                  <th>Status <small class="d-none d-md-inline"><a href="sort=status&order={{isset($_GET['order'])?$_GET['order']=='asc'?'desc':'asc' :'asc'}}"  class="text-dark sort"><i class="fa {{getSortIcon('status')}}"></i></a></small></th>
                  <th>Action</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Image</th>
                  <th>Name</th>
                  <th>Brand</th>
                  <th>Price($)</th>
                  <th>Year</th>
                  <th>Condition</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </tfoot>
              <tbody>
                @if(isset($cars) && count($cars) > 0)
                    @foreach($cars as $key => $car)
                        <tr>
                            <td class="text-center" style="width:100px">
                              @if($car->image)
                                <img width="100%" src="{{ asset('storage/images/'.$car->image)}}" alt="Image">
                              @else
                                <img width="100%" src="{{asset('assets/images/no_image.jpg')}}" alt="Image"> 
                              @endif
                            </td>
                            <td>{{$car->name}}</td>
                            <td>{{$car->brand->name}}</td>
                            <td>{{$car->price}}</td>
                            <td>{{$car->model_year}}</td>
                            <td class="text-center" style="width:120px;">{{$car->condition}}</td>
                            @if($car->status == 1) 
                              <td class="text-center"><span class='badge badge-success'>Enabled</span></td>
                            @else 
                              <td class="text-center"><span class='badge badge-danger'>Disabled</span></td>
                            @endif
                            <td>
                                <a href="{{route('admin.car.edit',$car->slug)}}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                <a href="{{route('admin.car.clone',$car->slug)}}" class="btn btn-success btn-sm"><i class="fa fa-clone"></i></a>
                                {!!Form::open(['route' => ['admin.car.destroy', $car->id], 'method' => 'POST', 'class' => 'd-inline'])!!}
                                   {{Form::hidden('_method','DELETE')}}
                                   <button type="submit" data-id={{$car->id}}  class="btn btn-danger btn-sm btn-delete-car" ><i class="fa fa-trash-o "></i></button>
                                 
                                {!!Form::close()!!}
                              </td>
                        </tr>
                    @endforeach
                @else
                  <td colspan="7">No data found</td>
                @endif
              </tbody>
            </table>
          </div>
          <span>Showing  {{ $cars->firstItem() }} - {{ $cars->lastItem() }} of {{$cars->total()}} entries,
            </span>
            Per page:
            <span class="text-dark">
              <a href="size=10" class="page text-dark {{!isset($_GET['size']) || $_GET['size'] == 10 ? 'font-weight-bold' : ''}}">10</a>, 
              <a href="size=25" class="page text-dark {{isset($_GET['size']) && $_GET['size'] == 25 ? 'font-weight-bold' : ''}}">25</a>, 
              <a href="size=50" class="page text-dark {{isset($_GET['size']) && $_GET['size'] == 50 ? 'font-weight-bold' : ''}}">50</a>,
              <a href="size=100" class="page text-dark {{isset($_GET['size']) && $_GET['size'] == 100 ? 'font-weight-bold' : ''}}">100</a>
            </span>
            <div class="float-right">
  
              @if ($cars->hasPages())
                  {{ $cars->appends(Request::except('page'))->links() }}
              @endif
            </div>
        </div>
      </div>     
</div>
@endsection