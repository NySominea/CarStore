@extends('admin.fragments.master')

@section('breadcrumb')
<!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.car.index')}}">Car</a></li>
        <li class="breadcrumb-item active">{{isset($car) && $car ? $car->name . " / Edit" : "New"}}</li>
    </ol>
@endsection

@section('content')
<div class="container-fluid pb-4">
    <div class="card mb-5">
        <div class="card-header">
          <strong>New Car</strong>  
        </div>
        <div class="card-body ">
          {!!Form::open(['route' => isset($car) && $car && !isset($clone) ? ['admin.car.update',$car->id] : 'admin.car.store', 'method' => 'POST', 'class' => 'form-inline', 'files' => true])!!}
            
            <div class="form-group col-md-12 mb-3 pl-0 pr-0 pl-0 pr-0">
              <div class="col-md-3 text-left  pl-0 pr-0">
                  {!!Form::label('name','Name *',['class' => 'justify-content-start'])!!}
              </div>
              <div class="col-md-9 pl-0 pr-0"> 
                  {!!Form::text('name', isset($car) && $car ? $car->name : '',['class' => 'form-control w-100 car-name', 'placeholder' => 'Car Name'])!!}
              </div>
            </div>

            <div class="form-group col-md-12 mb-3 pl-0 pr-0 pl-0 pr-0">
              <div class="col-md-3 text-left  pl-0 pr-0">
                  {!!Form::label('slug','Slug *',['class' => 'justify-content-start'])!!}
              </div>
              <div class="col-md-9 pl-0 pr-0"> 
                  {!!Form::text('slug', isset($car) && $car ? $car->slug : '',['class' => 'form-control w-100 car-slug', 'placeholder' => 'Slug Name'])!!}
              </div>
            </div>

            <div class="form-group col-md-12 mb-3 pl-0 pr-0 pl-0 pr-0">
              <div class="col-md-3 text-left  pl-0 pr-0">
                  {!!Form::label('brand','Brand *',['class' => 'justify-content-start'])!!}
              </div>
              <div class="col-md-9 pl-0 pr-0"> 
                  {!!Form::select('brand', $brands, isset($car) && $car ? $car->brand->id : null ,['class' => 'form-control w-100'])!!}
              </div>
            </div>

            <div class="form-group col-md-12 mb-3 pl-0 pr-0">
              <div class="col-md-3 text-left  pl-0 pr-0">
                  {!!Form::label('price','Price ($) *',['class' => 'justify-content-start'])!!}
              </div>
              <div class="col-md-9 pl-0 pr-0"> 
                  {!!Form::number('price',isset($car) && $car ? $car->price : '',['class' => 'form-control w-100', 'placeholder' => '10000.00'])!!}
              </div>
            </div>

            <div class="form-group col-md-12 mb-3 pl-0 pr-0">
              <div class="col-md-3 text-left  pl-0 pr-0">
                  {!!Form::label('model_year',' Model Year',['class' => 'justify-content-start'])!!}
              </div>
              <div class="col-md-9 pl-0 pr-0"> 
                  {!!Form::text('model_year',isset($car) && $car ? $car->model_year : '',['class' => 'form-control w-100', 'placeholder' => '2018'])!!}
              </div>
            </div>

            <div class="form-group col-md-12 mb-3 pl-0 pr-0 pl-0 pr-0">
                <div class="col-md-3 text-left  pl-0 pr-0">
                    {!!Form::label('color','Color',['class' => 'justify-content-start'])!!}
                </div>
                <div class="col-md-9 pl-0 pr-0"> 
                    {!!Form::text('color',isset($car) && $car ? $car->color : '',['class' => 'form-control w-100', 'placeholder' => 'White | Black'])!!}
                </div>
            </div>

            <div class="form-group col-md-12 mb-3 pl-0 pr-0 pl-0 pr-0">
                <div class="col-md-3 text-left  pl-0 pr-0">
                    {!!Form::label('fuel_type','Fuel Type',['class' => 'justify-content-start'])!!}
                </div>
                <div class="col-md-9 pl-0 pr-0"> 
                    {!!Form::text('fuel_type',isset($car) && $car ? $car->fuel_type : '',['class' => 'form-control w-100', 'placeholder' => 'LPG | Petrol | Dissel ...'])!!}
                </div>
            </div>

            <div class="form-group col-md-12 mb-3 pl-0 pr-0 pl-0 pr-0">
                <div class="col-md-3 text-left  pl-0 pr-0">
                    {!!Form::label('condition','Condition',['class' => 'justify-content-start'])!!}
                </div>
                <div class="col-md-9 pl-0 pr-0"> 
                    {!!Form::text('condition',isset($car) && $car ? $car->condition : '',['class' => 'form-control w-100', 'placeholder' => 'New | Used'])!!}
                </div>
            </div>

            <div class="form-group col-md-12 mb-3 pl-0 pr-0">
              <div class="col-md-3 text-left  pl-0 pr-0">
                  {!!Form::label('extra_data','Extra Information',['class' => 'd-flex align-items-start justify-content-start'])!!}
              </div>
              <div class="col-md-9 pl-0 pr-0"> 
                  {!!Form::textarea('description', isset($car) && $car ? $car->description : '', ['size' => '30x7','class' => 'form-control w-100', 'id' => 'summernote'])!!}
              </div>
            </div>

            <div class="form-group col-md-12 mb-3 pl-0 pr-0">
              <div class="col-md-3 text-left  pl-0 pr-0">
                  {!!Form::label('image','Image',['class' => 'd-flex justify-content-start'])!!}
              </div>
              <div class="col-md-9 pl-0 pr-0"> 
                <img id="image-upload" src="{{asset(isset($car->image)? 'storage/images/'.$car->image : 'assets/images/400x300.png')}}">
                @if(isset($clone))
                    <input hidden name="image_clone" type="text" value="{{$car->image}}">
                @else
                    <input hidden name="image" type="file" id="image">  
                @endif
                
              </div>
            </div>

            <div class="form-group col-md-12 mb-3 pl-0 pr-0">
                <div class="col-md-3 text-left  pl-0 pr-0">
                    {!!Form::label('image','Image Gallery',['class' => 'd-flex justify-content-start'])!!}
                </div>
                <div class="col-md-9 pl-0 pr-0 increment"> 
                    <div class="block-gallery d-inline">
                        
                    </div>
                    <img id="image-gallery" src="{{asset('assets/images/add-more.jpg')}}">
                    <input name="image-gallery[]" id="image-gallery-input" type="file" multiple hidden>
                </div>
            </div>
            
            <div class="form-group col-md-12 mb-3 pl-0 pr-0">
              <div class="col-md-3 text-left pl-0 pr-0">
                  {!!Form::label('status','Status',['class' => 'justify-content-start'])!!}
                </div>
                 <div class="col-md-9 pl-0 pr-0"> 
                    <label class="switch">
                        <input name="status" type="checkbox" {{isset($car) && $car ? $car->status ? "checked" : "" : "checked"}}>
                        <span class="slider"></span>
                        
                    </label>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <a href="{{ route('admin.car.index') }}" class="btn btn-danger text-white"><i class="fa fa-ban"></i> Cancel</a>
            {{isset($car) && $car && !isset($clone)? Form::hidden('_method','PUT') : ''}} 
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            @if(!isset($car) || isset($clone))
                <button id="save-continue" type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save & Continue</button>
            @endif
        </div>
        {!!Form::close()!!}
      </div>     
</div>
@endsection