<!DOCTYPE html>
<html lang="en">
<head>
<title>
    {{config('app.name','BookStore')}}
</title>
@include('admin.fragments.head')
</head>
<body class="bg-dark"
<div class="container">
  <div class="text-center text-white mt-5">
      <h1>{{config('app.name','BookStore')}}</h1>
  </div>
    <div class="card card-login mx-auto mt-3">
      <div class="card-header">{{ __('Login') }}</div>
      <div class="card-body">
        <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
          @csrf
          <div class="form-group">
            <label for="email">{{ __('E-Mail Address') }}</label>
            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

               @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
              @endif
          </div>
          <div class="form-group">
            <label for="password">{{ __('Password') }}</label>
            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

               @if ($errors->has('password'))
                   <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
          </div>
          <div class="form-group">
            <div class="form-check">
              <label class="form-check-label">
                  <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
            </div>
          </div>
          <button type="submit" class="btn btn-primary">
              {{ __('Login') }}
          </button>
        </form>
      </div>
    </div>
  </div> 
</body>
@include('admin.fragments.scripts')


