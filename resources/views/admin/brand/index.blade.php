@extends('admin.fragments.master')

@section('breadcrumb')
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active">Car Brand</li>
    </ol>
@endsection

@section('content')
<div class="container-fluid pb-4">
    
    <div class="card mb-5">
        <div class="card-header">
          <a href="{{route('admin.brand.create')}}" class="btn btn-primary text-white"><i class="fa fa-plus"></i> New Brand</a>
        </div>
        <div class="card-body">
          
            <div class="row mb-3">
              <div class="col-sm-12 col-md-4">
                <div class="form-inline" >
                  <label>Show &nbsp; 
                    <select id="brand_size" name="size_table" class="form-control form-control-sm">
                      <option {{$size == 10 ? 'selected' : ''}} value="size=10">10</option>
                      <option {{$size == 25 ? 'selected' : ''}} value="size=25">25</option>
                      <option {{$size == 50 ? 'selected' : ''}} value="size=50">50</option>
                      <option {{$size == 100 ? 'selected' : ''}} value="size=100">100</option>
                    </select> &nbsp; entries
                  </label>
                </div>
              </div> 
              <div class="col-sm-12 col-md-8 text-right">
                <form class="form-inline" method="GET" action="{{route('admin.brand.index')}}">
                      <label class="sr-only" for="Brand">Brand Name</label>
                      <input type="text" name="brand_name" class="form-control mb-2 mr-sm-3" id="brand-name" placeholder="Brand name"
                              value="{{isset($brand_name) && $brand_name ? $brand_name : ''}}">
                      
                      <label class="sr-only" for="Brand">Brand Status</label>
                      <select id="brand-status" name="brand_status" class="form-control mb-2 mr-sm-3">
                          <option value="all">Select Status</option>
                          <option {{isset($brand_status) && $brand_status == "enable" ? 'selected'  : ''}} value="enable">Enable</option>
                          <option {{isset($brand_status) && $brand_status == "disable" ? 'selected' : ''}} value="disable">Disable</option>
                      </select> 
                    
                      <button type="submit" class="btn btn-primary mb-2"><i class="fa fa-search"></i></button>
                    </form>
              </div>
            </div>
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Name <small><a href="sort=name&order={{isset($order)?$order=='asc'?'desc':'asc' :'asc'}}"  class="text-dark sort"><i class="fa {{getSortIcon('name')}}"></i></a></small></th>
                  <th>Status <small><a href="sort=status&order={{isset($order)?$order=='asc'?'desc':'asc' :'asc'}}" class="text-dark sort"><i class="fa {{getSortIcon('status')}}"></i></a></small></th>
                  <th>Car Count</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>Status</th>
                  <th>Car Count</th>
                  <th>Action</th>
                </tr>
              </tfoot>
              <tbody>
                @if(isset($brands) && count($brands) > 0)
                    @foreach($brands as $key => $brand)
                        <tr>
                            <td class="text-center">{{$key + 1}}</td>
                            <td>{{$brand->name}}</td>
                            @if($brand->status == 1) 
                              <td class="text-center"><span class='badge badge-success'>Enabled</span></td>
                            @else 
                              <td class="text-center"><span class='badge badge-danger'>Disabled</span></td>
                            @endif
                            <td class="text-center h5"><span class=" badge badge-info">{{count($brand->cars)}}</span></td>
                            <td>
                                <a href="{{route('admin.brand.edit',$brand->slug)}}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                <a href="{{route('admin.brand.clone',$brand->slug)}}" class="btn btn-success btn-sm"><i class="fa fa-clone"></i></a>
                                {!!Form::open(['route' => ['admin.brand.destroy', $brand->id], 'method' => 'POST', 'class' => 'd-inline'])!!}
                                   {{Form::hidden('_method','DELETE')}}
                                   <button type="submit" data-id={{$brand->id}} class="btn btn-danger btn-sm btn-delete-brand" id="delete"><i class="fa fa-trash-o "></i></button>
                                 
                                {!!Form::close()!!}
                            </td>
                        </tr>
                    @endforeach
                @else
                  <td colspan="5">No data found</td>
                @endif
              </tbody>
            </table>
          </div>

          <span>Showing  {{ $brands->firstItem() }} - {{ $brands->lastItem() }} of {{$brands->total()}} entries,
          </span>
          Per page:
          <span class="text-dark">
            <a href="size=10" class="page text-dark {{$size == 10 ? 'font-weight-bold' : ''}}">10</a>, 
            <a href="size=25" class="page text-dark {{$size == 25 ? 'font-weight-bold' : ''}}">25</a>, 
            <a href="size=50" class="page text-dark {{$size == 50 ? 'font-weight-bold' : ''}}">50</a>,
            <a href="size=100" class="page text-dark {{$size == 100 ? 'font-weight-bold' : ''}}">100</a>
          </span>
          <div class="float-right">

                  @if ($brands->hasPages())
                      {{ $brands->links() }}
                  @endif
          </div>
        </div>
      </div>     
</div>
@endsection