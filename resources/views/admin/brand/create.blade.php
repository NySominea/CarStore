@extends('admin.fragments.master')

<!-- Breadcrumbs-->
@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.brand.index')}}">Car Brand</a></li>
        <li class="breadcrumb-item active">{{isset($brand) && $brand ? $brand->name . " / Edit" : "New"}}</li>
    </ol>
@endsection

@section('content')
<div class="container-fluid pb-4">

    <div class="card">
        <div class="card-header">
            <strong>{{isset($brand) && $brand ? "Edit brand " : "New Book"}}  
           
        </div>
        
        <div class="card-body">      
            {!!Form::open(['route' => isset($brand) && $brand && !isset($clone) ? ["admin.brand.update",$brand->id] : 'admin.brand.store', 'method' => 'POST', 'class' => 'form-inline'])!!}       
            <div class="form-group col-md-12 mb-3 pl-0 pr-0">
              <div class="col-md-3 text-left pl-0">
                  {!!Form::label('name','Brand Name *',['class' => 'justify-content-start'])!!}
              </div>
              <div class="col-md-9"> 
                  {!!Form::text('name',isset($brand) && $brand ? $brand->name : "",['class' => 'form-control w-100 brand-name', 'placeholder' => 'Brand Name'])!!}
              </div>
            </div>

            <div class="form-group col-md-12 mb-3 pl-0 pr-0">
              <div class="col-md-3 text-left pl-0">
                  {!!Form::label('slug','Slug Name *',['class' => 'justify-content-start'])!!}
              </div>
              <div class="col-md-9"> 
                  {!!Form::text('slug',isset($brand) && $brand ? $brand->slug : "",['class' => 'form-control w-100 brand-slug', 'placeholder' => 'Slug Name'])!!}
              </div>
            </div>

            <div class="form-group col-md-12 mb-3 pl-0 pr-0">
                <div class="col-md-3 text-left pl-0">
                  {!!Form::label('status','Brand Status',['class' => 'justify-content-start'])!!}
                </div>
                 <div class="col-md-9"> 
                    <label class="switch">
                        <input name="status" type="checkbox" {{isset($brand) && $brand ? $brand->status ? "checked" : "" : "checked"}}>
                        <span class="slider"></span>
                        
                    </label>
                </div>
            </div>

        </div>
        <div class="card-footer">
            {{isset($brand) && $brand && !isset($clone) ? Form::hidden('_method','PUT') : ''}} 
            <a href="{{ route('admin.brand.index') }}" class="btn btn-danger text-white"><i class="fa fa-ban"></i> Cancel</a>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
           
            @if(!isset($brand) || isset($clone))
                <button id="save-continue" type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save & Continue</button>
            @endif
        </div>
        {!!Form::close()!!}
      </div>    
       
</div>
@endsection