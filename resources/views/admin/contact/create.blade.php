@extends('admin.fragments.master')

<!-- Breadcrumbs-->
@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active">Contact</li>
        
    </ol>
@endsection

@section('content')
<div class="container-fluid pb-4">

    <div class="card">
        <div class="card-header">
            <strong>Contact Us Information</strong> 
           
        </div>
        
        <div class="card-body">      
            {!!Form::open(['route' => isset($contact) && $contact ? ["admin.contact.update",$contact->id] : 'admin.contact.store', 'method' => 'POST', 'class' => 'form-inline'])!!}       
            <div class="form-group col-md-12 mb-3 pl-0 pr-0">
              <div class="col-md-3 text-left pl-0">
                  {!!Form::label('iframe_map','Iframe Map Source',['class' => 'justify-content-start'])!!}
              </div>
              <div class="col-md-9 pl-0 pr-0"> 
                  {!!Form::text('iframe_map',isset($contact) && $contact ? $contact->iframe_map : "",['class' => 'form-control w-100 brand-name', 'placeholder' => 'Iframe Map'])!!}
              </div>
            </div>

            <div class="form-group col-md-12 mb-3 pl-0 pr-0">
              <div class="col-md-3 text-left  pl-0 pr-0">
                  {!!Form::label('contact','Contact Info',['class' => 'd-flex align-items-start justify-content-start'])!!}
              </div>
              <div class="col-md-9 pl-0 pr-0"> 
                  {!!Form::textarea('contact', isset($contact) && $contact ? $contact->contact : '', ['size' => '30x7','class' => 'form-control w-100', 'id' => 'summernote'])!!}
              </div>
            </div>

        </div>
        <div class="card-footer">
            {{isset($contact) && $contact ? Form::hidden('_method','PUT') : ''}} 
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
           
        </div>
        {!!Form::close()!!}
      </div>    
       
</div>
@endsection