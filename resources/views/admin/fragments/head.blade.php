
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<!-- Bootstrap core CSS-->
<link href="{{asset('adminAssets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="{{asset('adminAssets/vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="{{asset('adminAssets/vendor/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="{{asset('adminAssets/css/sb-admin.css')}}" rel="stylesheet">

<link href="{{asset('adminAssets/css/custom.css')}}" rel="stylesheet">