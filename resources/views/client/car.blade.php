@extends('fragments.master')

@section('title')
    Home
@endsection

@section('breadcrumb')
<nav aria-label="breadcrumb " class="bg-light">
    <ol class="breadcrumb rounded-0 container bg-light">
        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
        @if(!isset($brand))
        <li class="breadcrumb-item active" aria-current="page">Car</li>
        @else
        <li class="breadcrumb-item active" aria-current="page"><a href="{{route('car')}}">Car</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{$brand->name}}</li>
        @endif
    </ol>
</nav>
@endsection

@section('content') 
    <h5>
    @if($_GET)
        Search result of ( {{isset($_GET['name']) ? 'Name = "'. $_GET['name'] . '", ' : ''}}
        {{isset($_GET['brand']) ? 'brand = "'.$brand->name.'' . '"' : ''}}
        {{isset($_GET['author']) ? 'Author = "'. $_GET['author'] . '",' : ''}}
        {{isset($_GET['from']) ? 'From = "'. $_GET['from'] . '",' : ''}} 
        {{isset($_GET['to']) ? 'To = "'. $_GET['to'] . '",' : ''}})
    @endif
    </h5>

    @if(isset($cars) && count($cars) > 0)
    <div class="row">
        @foreach($cars as $car)
            <div class="col-md-4 mb-4 col-sm-6 col-6">
                <a href="{{route('detail',$car->id )}}" id="view-car" data-id={{$car->id}} style="text-decoration:none;" class="text-left p-0">
                    <div class="card ">
                        <img class="card-img-top mw-100" src="{{asset('storage/images/'.$car->image)}}" alt="Car Image">
                        <div class="card-body p-2">
                            <h5 class="card-title text-dark">{{$car->name}} ({{$car->published_year}})</h5>
                            <hr class="border border-secondary border-bottom-0  mt-1 mb-1"></hr>
                            <p class="m-0 text-dark">{{$car->author}}</p>
                        </div>
                    </div>
                </a>     
            </div>  
        @endforeach
    </div>
    @else
        No Car Found
    @endif
    @if ($car->hasPages())
        {{ $car->appends(Request::except('page'))->links() }}
    @endif
@endsection