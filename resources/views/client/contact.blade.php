@extends('fragments.master')

@section('title')
    About Us
@endsection

@section('breadcrumb')
<nav aria-label="breadcrumb " class="bg-light">
<ol class="breadcrumb rounded-0 container bg-light">
    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
</ol>
</nav>
@endsection

@section('content') 
    <div class="row">
        @if($contact)
            <iframe src="{{ $contact->iframe_map }}" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
        @endif
            
        <div class="container-fluid">
            {!! $contact->contact !!}
        </div>
    </div>
@endsection