@extends('fragments.master')

@section('title')
    Home
@endsection

@section('breadcrumb')
<nav aria-label="breadcrumb " class="bg-light">
    <ol class="breadcrumb rounded-0 container bg-light">
        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{route('brand',$car->brand->id)}}">{{$car->brand->name}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{$car->name}}</li>

    </ol>
</nav>
@endsection

@section('content') 
    @if(isset($car))
        <div class="row">
            <div class="col-sm-7 text-center mb-4">
                <div class="owl-carousel owl-theme" data-loop="true" data-nav="false" data-autoplay="true" data-dots="true" data-nav="true" data-autoplaytimeout="3000">
                    <img src="{{asset('storage/images/'.$car->image)}}" class="mw-100 w-100">
                    <img src="{{asset('storage/images/'.$car->image)}}" class="mw-100 w-100">
                </div>
            </div>
            <div class="col-sm-5">
                <h3 class="font-weight-bold mb-4">{{$car->name}}</h3>
                <table class="table"> 
                    <tbody>
                        @if($car->name)
                        <tr>
                            <td class="w-25 border-top border-secondary"><strong>Brand</strong></td> <td class="border-top border-dark">{{$car->brand->name}}</td>
                        </tr>
                        @endif

                        @if($car->price)
                        <tr>
                            <td class="w-25 border-top border-secondary"><strong>Price</strong></td> <td class="border-top border-dark">${{number_format($car->price, 2, '.', ',')}}</td>
                        </tr>
                        @endif

                        @if($car->model_year)
                        <tr>
                            <td class="w-25 border-top border-secondary"><strong>Year</strong></td> <td class="border-top border-dark">{{$car->model_year}}</td>
                        </tr>
                        @endif

                        @if($car->condition)
                        <tr>
                            <td class="w-25 border-top border-secondary"><strong>Condition</strong></td> <td class="border-top border-dark">{{$car->condition}}</td>
                        </tr>
                        @endif

                        @if($car->fuel_type)
                        <tr>
                            <td class="w-25 border-top border-secondary"><strong>Fuel Type</strong></td> <td class="border-top border-dark">{{$car->fuel_type}}</td>
                        </tr>
                        @endif

                        @if($car->color)
                        <tr>
                            <td class="w-25 border-top border-secondary"><strong>Color</strong></td> <td class="border-top border-dark">{{$car->color}}</td>
                        </tr>
                        @endif
                        
                    </tbody>
                </table>
            </div>
            @if($car->description)
                <div class="col-md-12">
                    <b>EXTRA INFORMATION</b> <br>
                    {!! $car->description !!}
                </div>
            @endif
        </div>

        
    @endif
@endsection