@extends('fragments.master')

@section('title')
    Home
@endsection

@section('breadcrumb')
    <nav aria-label="breadcrumb " class="bg-light">
    <ol class="breadcrumb rounded-0 container bg-light">
        
        @if(isset($brand))
            <li class="breadcrumb-item" aria-current="page"><a href="{{route('home')}}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Car</li>
            <li class="breadcrumb-item active" aria-current="page">{{$brand->name}}</li>
        @else
            <li class="breadcrumb-item active" aria-current="page">Home</li>    
        @endif
        
    </ol>
    </nav>
@endsection

@section('content') 

    @if(isset($cars) && count($cars) > 0)
    <div class="row" id="car-list">
        @foreach($cars as $car)
            <div class="col-md-3 mb-2 col-sm-6 col-6">
                <a href="{{route('detail',$car->slug )}}" id="view-car" data-id={{$car->id}} style="text-decoration:none;" class="text-left p-0">
                    <div class="card ">
                        <div class="card-img position-relative">
                            @if($car->image)
                                <img class="card-img-top mw-100" src="{{asset('storage/images/'.$car->image)}}" alt="Car Image">
                            @else
                                <img class="card-img-top mw-100" src="{{asset('assets/images/no_image.jpg')}}" alt="Car Image">
                            @endif
                            <span class="position-absolute condition-label">{{$car->condition}}</span>
                        </div>
                        <div class="card-body p-2">
                            <div class="car-price">${{number_format($car->price, 2, '.', ',')}}</div>
                            <div class="card-title text-dark h5">{{$car->name}}</div>
                        </div>
                    </div>
                </a>     
            </div>  
        @endforeach
    </div>
    @else
        No Car Found
    @endif
    @if ($cars->hasPages())
        {{ $cars->appends(Request::except('page'))->links() }}
    @endif
@endsection