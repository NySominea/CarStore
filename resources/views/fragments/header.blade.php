<nav class="navbar navbar-expand-md bg-dark navbar-dark">
    <div class="container">
        <a class="navbar-brand mb-1 mr-4 font-weight-bold" href="{{route('home')}}">{{config('app.name','CarStore')}}</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{route('contact')}}">Contact Us</a>
            </li>   
        </ul>
        
        {!!Form::open(['route' => 'searchCarName', 'method' => 'GET', 'class' => 'form-inline my-2 my-lg-0'])!!}
            {!!Form::text('name',isset($_GET['name']) ? $_GET['name'] : '',['type' => 'search', 'aria-label' => 'Search', 'class' => 'form-control mr-sm-2', 'placeholder' => 'Search car by name'])!!}
            {!!Form::submit('Search',['class' => 'btn btn-outline-primary my-2 my-sm-0'])!!}
        {!!Form::close()!!}
        </div>  
    </div>
  </nav>