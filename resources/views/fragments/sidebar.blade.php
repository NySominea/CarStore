<div class="col-lg-3 mb-4 order-lg-1 p-0">
    @if(isset($brands) && count($brands) > 0)
        <div class="col-lg-12 mb-4">
            <div class="list-group brand-list"> 
                <p class="h5 list-group-item list-group-item-action bg-light font-weight-bold">Car Brand</p> 
            @foreach($brands as $brand)
                @if(count($brand->cars) > 0)
                <a href="{{route('brand',$brand->slug)}}" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center {{isset($slug) && $slug==$brand->slug ? 'current': ''}}">
                   {{$brand->name}}
                   <span class="badge badge-primary badge-pill">{{$brand->countCar()}}</span>
                </a>
                @endif
            @endforeach
            </div> 
        </div>
    @endif
    <div class="col-lg-12">
        <div class="card">
        <div class="card-header p-3">
            <b class="h5 font-weight-bold">Filter</b>
        </div>
        {!!Form::open(['route' => 'filter', 'method' => 'get'])!!}
        <div class="card-body p-3">
            
            <div class="form-group">
                {!!Form::label('name', 'Car Name');!!}
                {!!Form::text('name', isset($_GET['name']) ? $_GET['name'] : '' ,['class' => 'form-control']);!!}
            </div>
            <div class="form-group">
                {!!Form::label('author', 'Brand');!!}
                <select name="brand" class="form-control w-100">
                    <option value="all">All Brand</option>
                    @foreach($brands as $brand)
                        @if(count($brand->cars)>0)
                            <option value="{{$brand->slug}}" {{isset($_GET['brand'])&&$_GET['brand']==$brand->slug?'selected':''}}>{{$brand->name}}</option>
                        @endif
                    @endforeach
                </select>
                
            </div>
            <div class="form-group">
                {!!Form::label('price', 'Price ($)');!!}
                <div class="row">
                    <div class="col-md-6">
                        {!!Form::label('from', 'From');!!}
                        {!!Form::text('from',isset($_GET['from']) ? $_GET['from'] : '',['class' => 'form-control ']);!!}
                    </div>
                    <div class="col-md-6">
                        {!!Form::label('to', 'To');!!}
                        {!!Form::text('to',isset($_GET['to']) ? $_GET['to'] : '',['class' => 'form-control']);!!}
                    </div>
                </div>
            </div>
            
        </div>
        <div class="card-footer">
            {!!Form::submit('Filter',['class' => 'btn btn-primary w-100'])!!}
        </div>
        {!!Form::close()!!}
    </div>
    </div>
</div>