<!DOCTYPE html>
<html lang="en">
<head>
    
<title>      
   {{config('app.name','BookStore')}} | @yield('title')

</title>
<link rel="icon" type="image/jpg" href="{{ asset('assets/images/site.jpg') }}">
@include('fragments.head')
</head>
<body>
<div class="app">
    @include('fragments.header')
    @yield('breadcrumb')
    
    <div class="container">
        <div class="row mt-4">
            <div class="col-lg-9 mb-4 order-lg-12">
                @yield('content')
            </div>

            @include('fragments.sidebar')
            
        </div>
        
    </div>

    @include('fragments.footer')
</div>
</body>
@include('fragments.scripts')


