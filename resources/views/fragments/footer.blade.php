<div class="bg-dark text-center text-white m-0 p-3">
    <p class="h5 font-weight-bold mb-0 text-primary">{{config('app.name','CarStore')}}</p>
    <hr class="w-50 border-white">
    <p class="mb-0">All Rights Reserved 2018</p>
</div>