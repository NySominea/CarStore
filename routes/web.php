<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/admin/login',['uses' => 'AdminController@login', 'as' => 'admin.login']);
Route::post('/admin/logout',['uses' => 'AdminController@logout', 'as' => 'admin.logout']);

Route::group(['prefix' => '/admin', 'middleware' => 'auth' ], function () {
    Route::get('dashboard',['uses' => 'AdminController@index', 'as' => 'admin.dashboard']);

    Route::resource('car','CarController',['as' => 'admin']);
    Route::get('/car/{car_id}/clone','CarController@clone')->name('admin.car.clone');

    Route::resource('brand','BrandController',['as' => 'admin']);
    Route::get('/brand/{brand_slug}/clone','BrandController@clone')->name('admin.brand.clone');

    Route::resource('user','UserController',['as' => 'admin']);
    Route::resource('contact', 'ContactController', ['as' => 'admin']);
});


Route::get('/',['uses' => 'FrontendController@index', 'as' => 'home']);
Route::get('/contact-us',['uses' => 'FrontendController@contact', 'as' => 'contact']);
Route::get('car/filter',['uses' => 'FrontendController@filter', 'as' => 'filter']);
Route::get('car/search',['uses' => 'FrontendController@searchCarName', 'as' => 'searchCarName']);
Route::get('/car/brand/{slug}', ['uses' => 'FrontendController@carByBrand', 'as' => 'brand']);
Route::get('/car/{slug}',['uses' => 'FrontendController@carDetail', 'as' => 'detail']);



// Route::get('/home', 'HomeController@index')->name('home');

