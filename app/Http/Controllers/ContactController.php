<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contact = Contact::first();
        return view('admin.contact.create')->with('contact',$contact);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $iframe_map  =   $request->input('iframe_map');
        $contact_info  =   $request->input('contact');

        $contact =  new Contact;
        $contact->iframe_map    =   $iframe_map;
        $contact->contact   =   $contact_info;
        $contact->save();

        return redirect()->route('admin.contact.index')->with([
            'success' => "Your contact information is successfully created."
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $iframe_map  =   $request->input('iframe_map');
        $contact_info  =   $request->input('contact');

        $contact =  Contact::findOrFail($id);
        $contact->iframe_map    =   $iframe_map;
        $contact->contact   =   $contact_info;
        $contact->save();

        return redirect()->route('admin.contact.index')->with([
            'success' => "Your contact information is successfully created."
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
