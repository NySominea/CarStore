<?php

namespace App\Http\Controllers;

use App\Car;
use Illuminate\Http\Request;
use App\Brand;
use Intervention\Image\ImageManagerStatic as Image;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $brand_id = '';
        $size =$request->input('size');
        if(!$size){
            $size = 10   ;
        }

        $sort = $request->input('sort');
        $order = $request->input('order');
        if($sort && $order){
            if($sort == 'brand'){
                $cars = Car::orderBy('brands.name',$order)
                        ->join('brands','brands.id','=','cars.brand_id')
                        ->select('cars.*');
            }else{
                $cars = Car::orderBy($sort,$order);
            }
            
        }else{
            $cars = Car::orderBy('cars.created_at','desc');
        }

        $search = $request->input('search');
        $brand_slug = $request->input('car_brand');
        $car_status = $request->input('car_status');

        if($search){
             $cars->where(function($cars) use ($search){
                $cars->orWhere('cars.name','LIKE','%'.$search.'%')
                ->orWhere('cars.price',$search)
                ->orWhere('cars.model_year','LIKE', '%'.$search.'%');
                });
        }
        if($brand_slug!= null){
            if($brand_slug != 'all'){
                $brand = Brand::where('slug',$brand_slug)->first();
                $cars->where('brand_id',$brand->id);
            }   
        } 

        if($car_status != null){
            if($car_status != 'all'){
                $cars->where('cars.status',$car_status == "enable" ? 1 : 0);
             } 
        }    
        
        $cars = $cars->paginate($size);
        
        $brands = Brand::all();
        return view('admin.car.index')->with([
            'cars' => $cars,
            'brands' => $brands,  
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands = Brand::where('status',1)->orderBy('name','ASC')->pluck('name','id');
        
        return view('admin.car.create')->with([
            'brands' => $brands
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'slug' => 'unique:cars,slug',
            'price' => 'required | numeric',
            'model_year' => 'nullable | numeric',
            'image' => 'image | mimes:jpg,jpeg,png',
        ]);

        if($request->hasFile('image')){
            $image = $request->file('image');
            
            $imageNameWithExt = $image->getClientOriginalName();

            $imageName = pathInfo($imageNameWithExt,PATHINFO_FILENAME);

            $extension = $image->getClientOriginalExtension();

            $newImageName = $imageName.'_'.time().'.'.$extension;

            $pathImage = $image->storeAs('public/images',$newImageName);

            // Resize

            $thumbnailpath = public_path('storage/images/'.$newImageName);

            Image::make($thumbnailpath)->resize(400, 300)->save($thumbnailpath);

        }


        $car = new Car;
        $car->name = $request->input('name');
        $car->slug = $request->input('slug');
        $car->brand_id = $request->input('brand');
        $car->price = $request->input('price');
        $car->fuel_type = $request->input('fuel_type');
        $car->condition = $request->input('condition');
        $car->color = $request->input('color');
        $car->description = $request->input('description');
        $car->model_year = $request->input('model_year');
        if(isset($newImageName)){
            $car->image = $newImageName;
        }else if($request->input('image_clone')){
            $car->image   =  $request->input('image_clone');
        } 
        $car->status = $request->input('status') == "on" ? 1 : 0;
        $car->user_id = auth()->user()->id;

        $car->save();

        if($request->input('save_continue')){
            return redirect()->route('admin.car.create')->with([
                'success' => $car->name . " car has been created."
            ]);
        }

        return redirect()->route('admin.car.index')->with([
            'success' => $car->name . " car has been created."
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function show(Car $car)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $brands = Brand::where('status',1)->orderBy('name','ASC')->pluck('name','id');
        
        $car = Car::where('slug',$slug)->first();

        return view('admin.car.create')->with([
            'car' => $car,
            'brands' => $brands
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'slug' => 'unique:cars,slug,'.$id,
            'price' => 'required | numeric',
            'model_year' => 'nullable | numeric',
            'image' => 'image | mimes:jpg,jpeg,png',
        ]);

        if($request->hasFile('image')){
            $image = $request->file('image');
            
            $imageNameWithExt = $image->getClientOriginalName();

            $imageName = pathInfo($imageNameWithExt,PATHINFO_FILENAME);

            $extension = $image->getClientOriginalExtension();

            $newImageName = $imageName.'_'.time().'.'.$extension;

            $pathImage = $image->storeAs('public/images',$newImageName);

            // Resize

            $thumbnailpath = public_path('storage/images/'.$newImageName);

            Image::make($thumbnailpath)->resize(400, 300)->save($thumbnailpath);

        }


        $car = Car::findOrFail($id);
        $car->name = $request->input('name');
        $car->slug = $request->input('slug');
        $car->brand_id = $request->input('brand');
        $car->price = $request->input('price');
        $car->fuel_type = $request->input('fuel_type');
        $car->condition = $request->input('condition');
        $car->color = $request->input('color');
        $car->description = $request->input('description');
        $car->model_year = $request->input('model_year');
        if(isset($newImageName)) $car->image = $newImageName;
        $car->status = $request->input('status') == "on" ? 1 : 0;
        $car->user_id = auth()->user()->id;

        $car->save();

        if($request->input('save_continue')){
            return redirect()->route('admin.car.create')->with([
                'success' => $car->name . " car has been updated."
            ]);
        }

        return redirect()->route('admin.car.index')->with([
            'success' => $car->name . " car has been updated."
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $car = Car::findOrFail($id);
        $name = $car->title;
        $car->delete();

        return redirect()->route('admin.car.index')->with([
            'success' => $name . ' book has been successfuly deleted.'
        ]);
    }

    public function clone($slug){
        $brands = Brand::where('status',1)->orderBy('name','ASC')->pluck('name','id');

        $car = Car::where('slug',$slug)->first();

        return view('admin.car.create')->with([
            'car' => $car,
            'brands' => $brands,
            'clone' => 'clone'
        ]);
    }
}
