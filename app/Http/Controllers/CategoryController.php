<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        // $categories = Category::where('');

        $size =$request->input('size');
        if(!$size){
            $size = 10   ;
        }

        $sort = $request->input('sort');
        $order = $request->input('order');
        if($sort && $order){
            
            $categories = Category::orderBy($sort,$order);
        }else{
            $categories = Category::orderBy('created_at','desc');
        }

        $category_name = $request->input('category_name');
        $category_status = $request->input('category_status');
        if($category_name){
            $categories->where('name','LIKE','%'.$category_name.'%');
        }
        if($category_status){
            if($category_status != 'all'){
                $categories->where('status',$category_status == "enable" ? 1 : 0);
            }
        }
        // $categories->orderBy($sort,$order);
        $categories = $categories->paginate($size);
        return view('admin.category.index')->with([
            'categories' => $categories,
            'size' => $size,
            'category_name' => $category_name,
            'category_status' => $category_status,
            'sort' => $sort,
            'order' => $order
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
        ]);

        $category = new Category;
        $category->name = $request->input('name');
        $category->status = $request->input('status') ? 1 : 0;
        $category->user_id = auth()->user()->id;
        $category->save();
        
        if($request->input('save_continue')){
            return redirect()->route('admin.category.create')->with([
                'success' => $category->name . " category is successfully created."
            ]);
        }

        return redirect()->route('admin.category.index')->with([
            'success' => $category->name  . ' category is successfully created.'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $category = Category::findOrFail($id);

        return view('admin.category.create')->with([
            'category' => $category
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);

        $this->validate($request,[
            'name' => 'required',
        ]);
       
        $category->name = $request->input('name');
        $category->status = $request->input('status') == "on" ? 1 : 0;
        $category->save();

        return redirect()->route('admin.category.index')->with([
            'success' => $category->name . " category has been successfully updated."
        ]);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $name = $category->name;
        $category->books()->delete();
        $category->delete();

        return redirect()->route('admin.category.index')->with([
            'success' => $name . ' category has been successfuly deleted.'
        ]);
    }

    public function clone($category_id){
        $category = Category::findOrFail($category_id);

        return view('admin.category.create')->with([
            'category' => $category,
            'clone' => 'clone'
        ]);
    }
}
