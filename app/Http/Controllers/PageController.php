<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Category;
class PageController extends Controller
{
    public function index(){
        
        $categories = Category::orderBy('name','asc')->where('status',1)->get();
        return view('client.index')->with([
            'categories' => $categories,
        ]);
    }

    public function book(){
        $books = Book::orderBy('created_at','desc')->where('status',1)->paginate(6);
        $categories = Category::orderBy('name','asc')->where('status',1)->get();
        return view('client.book')->with([
            'books' => $books,
            'categories' => $categories
        ]);
    }

    public function bookDetail($id){
        $categories = Category::orderBy('name','asc')->where('status',1)->get();

        $book = Book::findOrFail($id);
   
        return view('client.detail')->with([
            'book' => $book,
            'categories' => $categories
        ]);
    }

    public function bookByCategory($id){
        $category = Category::find($id);
        $categories = Category::orderBy('name','asc')->where('status',1)->get();
        $books = $category->books()->where('status',1)->paginate(6);
        // $books = Book::where('category_id',$id)->where('status',1)->paginate(6);
        return view('client.book')->with(
            [
                'books' => $books,
                'categories' => $categories,
                'category' => $category
            ]
        );
    }
    public function about(){
        $categories = Category::orderBy('name','asc')->where('status',1)->get();
        return view('client.about')->with('categories',$categories);
    }

    public function contact(){
        $categories = Category::orderBy('name','asc')->where('status',1)->get();
        return view('client.contact')->with('categories',$categories);
    }
    public function search(Request $request){
        $categories = Category::orderBy('name','asc')->where('status',1)->get();

        $input = $request;  
        $title = $input->input('title');
        $author = $input->input('author');
        $from = $input->input('from');
        $to = $input->input('to');

        if(!$title && !$author && !$from && !$to){
            return redirect()->route('book');
        }

        $books = Book::where('status',1);
        $books = $title ? $books->where('title', 'LIKE', '%'.$title.'%') : $books;
        $books = $author ? $books->where('author', 'LIKE', '%'. $author. '%') : $books;
        $books = $from && $to ? $books->whereBetween('published_year', [$from,$to]) : $books;
        $books = $books->paginate(6);

        return view('client.book')->with([
            'categories' => $categories,
            'books' => $books
        ]);
    }

    public function searchByCategory(Request $request){
        $input = $request;
        $title = $input->input('title');
        $category_id = $input->input('category');
        $category = Category::find($category_id);
        $categories = Category::orderBy('name','asc')->where('status',1)->get();

        if(!$title && !$category_id){
            return redirect()->route('book');
        }

        $books = Book::where('status',1)->where('title','LIKE','%'.$title.'%');
        $books = $category_id ? $books->where('category_id',$category_id) : $books;
        $books = $books->paginate(6);

        return view('client.book')->with([
            'books' => $books,
            'categories' => $categories,
            'category' => $category
        ]);
    }
    public function countDownload($id){
        $book = Book::find($id);
        $book->download += 1;
        $book->save();
        return true;
    }

    public function countView($id){
        $book = Book::find($id);
        $book->view += 1;
        $book->save();
        return true;
    }
}
