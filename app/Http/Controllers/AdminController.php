<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Car;
use App\User;
use App\Brand;
use App\Contact;
class AdminController extends Controller
{

    public function index(){
        $car_count = Car::count();
        $brand_count = Brand::count();
        $user_count = User::count();
        $data = [
            'car' => $car_count,
            'brand' => $brand_count,
            'user' => $user_count
        ];
        return view('admin.index')->with('total',$data);
    }


    public function login(){
        return view('admin.login');
    }

    public function logout(){
        Auth::logout();
        return redirect()->route('admin.login');
    }
}
