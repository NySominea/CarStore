<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;
use App\Car;
use App\Contact;
use \Illuminate\Database\Eloquent\ModelNotFoundException;

class FrontendController extends Controller
{
    private $per_page;

    public function __construct(){
        $this->per_page   =   20;
    }
    public function index(){
        
        $cars   = Car::where('status',1)->orderBy('created_at','DESC')->paginate($this->per_page);
        $brands = Brand::orderBy('name','asc')->where('status',1)->get();
        return view('client.index')->with([
            'cars' => $cars,
            'brands' => $brands
        ]);
    }

    public function car(){
        $cars = Car::orderBy('created_at','desc')->where('status',1)->paginate($this->per_page);
        $brands = Brand::orderBy('name','asc')->where('status',1)->get();
        return view('client.brand')->with([
            'cars' => $cars,
            'brands' => $brands
        ]);
    }

    public function carDetail($slug){
        $brands = Brand::orderBy('name','asc')->where('status',1)->get();

        $car = Car::where('slug',$slug)->where('status',1)->first();;
   
        return view('client.detail')->with([
            'car' => $car,
            'brands' => $brands
        ]);
    }

    public function carByBrand($slug){
        $brand = Brand::where('slug',$slug)->first();
        if(!$brand){
            throw new ModelNotFoundException;
        }
        $brands = Brand::orderBy('name','asc')->where('status',1)->get();
        $cars = $brand->cars()->where('status',1)->orderBy('created_at','DESC')->paginate($this->per_page);

        return view('client.index')->with(
            [
                'cars' => $cars,
                'brands' => $brands,
                'brand' => $brand,
                'slug' => $slug 
            ]
        );
    }

    public function contact(){
        $brands = Brand::orderBy('name','asc')->where('status',1)->get();
        $contact= Contact::first();
        return view('client.contact')->with([
            'brands'=> $brands,
            'contact' => $contact
        ]);
    }
    public function filter(Request $request){
        $brands = Brand::orderBy('name','asc')->where('status',1)->get();

        $input = $request;  
        $name = $input->input('name');
        $brand_slug = $input->input('brand');
        $from = $input->input('from');
        $to = $input->input('to');

        if(!$name && !$brand_slug && !$from && !$to){
            return redirect()->back();
        }

        if($brand_slug && $brand_slug!='all'){
            $brand  =   Brand::where('status',1)->where('slug',$brand_slug)->first();
        }
        $cars = Car::where('status',1);
        $cars = $name ? $cars->where('name', 'LIKE', '%'.$name.'%') : $cars;
        $cars = isset($brand) ? $cars->where('brand_id', $brand->id) : $cars;

        if($from && $to){
            $cars =  $cars->whereBetween('price', [$from,$to]);
        }else{
            if($from){
                $cars =  $cars->where('price', '>=' ,$from);
            }else if($to){
                $cars =  $cars->where('price', '<=' ,$to);
            }
        }
        
        $cars = $cars->paginate($this->per_page);

        return view('client.index')->with([
            'brands' => $brands,
            'cars' => $cars
        ]);
    }

    public function searchCarName(Request $request){
        $input = $request;
        $name = $input->input('name');

        $cars = Car::where('status',1)->where('name','LIKE','%'.$name.'%')->paginate($this->per_page);
        $brands = Brand::orderBy('name','asc')->where('status',1)->get();

        return view('client.index')->with([
            'cars' => $cars,
            'brands' => $brands,
        ]);
    }
}
