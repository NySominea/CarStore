<?php

namespace App\Http\Controllers;

use App\Brand;
use Illuminate\Http\Request;
use \Illuminate\Database\Eloquent\ModelNotFoundException;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $size =$request->input('size');
        if(!$size){
            $size = 10   ;
        }

        $sort = $request->input('sort');
        $order = $request->input('order');
        if($sort && $order){
            $brands = Brand::orderBy($sort,$order);
        }else{
            $brands = Brand::orderBy('created_at','desc');
        }

        $brand_name = $request->input('brand_name');
        $brand_status = $request->input('brand_status');
        if($brand_name){
            $brands->where('name','LIKE','%'.$brand_name.'%');
        }
        if($brand_status){
            if($brand_status != 'all'){
                $brands->where('status',$brand_status == "enable" ? 1 : 0);
            }
        }
        
        $brands = $brands->paginate($size);
        return view('admin.brand.index')->with([
            'brands' => $brands,
            'size' => $size,
            'brand_name' => $brand_name,
            'brand_status' => $brand_status,
            'sort' => $sort,
            'order' => $order
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.brand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'slug' => 'required | unique:brands,slug'
        ]);

        $brand = new Brand;
        $brand->name = $request->input('name');
        $brand->slug = $request->input('slug');
        $brand->status = $request->input('status') ? 1 : 0;
        $brand->save();
        
        if($request->input('save_continue')){
            return redirect()->route('admin.brand.create')->with([
                'success' => $brand->name . " brand is successfully created."
            ]);
        }

        return redirect()->route('admin.brand.index')->with([
            'success' => $brand->name  . ' brand is successfully created.'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $brand = Brand::where('slug',$slug)->first();

        if(!$brand){
            throw new ModelNotFoundException;
        }
        return view('admin.brand.create')->with([
            'brand' => $brand
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $brand = Brand::find($id);
        
        $this->validate($request,[
            'name' => 'required',
            'slug' => 'required | unique:brands,slug,'.$id
        ]);
       
        $brand->name = $request->input('name');
        $brand->slug = $request->input('slug');
        $brand->status = $request->input('status') == "on" ? 1 : 0;
        $brand->save();

        return redirect()->route('admin.brand.index')->with([
            'success' => $brand->name . " brand has been successfully updated."
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand = Brand::findOrFail($id);
        $name = $brand->name;
        $brand->cars()->delete();
        $brand->delete();

        return redirect()->route('admin.brand.index')->with([
            'success' => $name . ' brand has been successfuly deleted.'
        ]);
    }

    public function clone($slug){
        $brand = Brand::where('slug',$slug)->first();

        if(!$brand){
            throw new ModelNotFoundException;
        }

        return view('admin.brand.create')->with([
            'brand' => $brand,
            'clone' => 'clone'
        ]);
    }
}
