<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Car;

class Brand extends Model
{
    public function cars(){
        return $this->hasMany(Car::class);
    }

    public function countCar(){
        return $this->cars()->where('status',1)->count();
    }

    public function getAmountOfCars($number){
        return $this->cars()->take($number)->get();
    }

}
